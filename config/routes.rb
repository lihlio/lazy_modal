LazyModal::Engine.routes.draw do
  resources :lazy_modals, only: [:show]
end
