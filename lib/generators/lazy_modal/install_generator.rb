module LazyModal
  class InstallGenerator < Rails::Generators::Base

    desc 'install lazy modal'
    def install
      route("mount LazyModal::Engine => '/'")
    end

    def inject_js_asset_file
      %w(js coffee).each do |ext|
        dest_file_path = "app/assets/javascripts/application.#{ext}"
        if File.exist? "#{destination_root}/#{dest_file_path}"
          insert_into_file dest_file_path, :before => "//= require_tree" do
            "//= require lazy_modal/application\n"
          end
          break
        end
      end
    end

    def inject_css_asset_file
      %w(css scss sass).each do |ext|
        dest_file_path = "app/assets/stylesheets/application.#{ext}"
        if File.exist? "#{destination_root}/#{dest_file_path}"
          insert_into_file dest_file_path, :before => "*= require_self" do
            "*= require lazy_modal/application\n"
          end
          break
        end
      end
    end

  end
end
