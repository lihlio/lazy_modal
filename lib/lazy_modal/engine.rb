require 'bootstrap-sass'
require 'jquery-rails'
module LazyModal
  class Engine < ::Rails::Engine
    isolate_namespace LazyModal
  end
end
