# lazy-modal

show `bootstrap` modal with lazy load. sometimes we have too many modals in html body. and u know always some modal not need rendered. the best pratice to show modal i think is `lazy load modal`. 
`bootstrap` modal plugin provide remote away the load modal content when needed.
this gem provide way load `bootstrap` modal from remote server. so that we can load the modal when needed it. it's start!  

## Usage
How to `lazy-modal`? too simplest!

## Installation
Add this line to your rails application's Gemfile:

```ruby
gem 'lazy_modal'
```

And then execute:

```bash
$ bundle
```

Install required file

```
$ rails generate lazy_modal:install
```

That's all!

Now test if `lazy-modal` installed?

Start server

```bash
$ rails s 
```

open below url in any browse [http://localhost:3000/lazy_modal/demo](http://localhost:3000/lazy_modal/demo)

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
