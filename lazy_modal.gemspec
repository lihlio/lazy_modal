$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'lazy_modal/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'lazy_modal'
  s.version     = LazyModal::VERSION
  s.authors     = ['MixBo']
  s.email       = ['lb563@foxmail.com']
  s.homepage    = 'http://mixbo.tech'
  s.summary     = 'lazy load modals'
  s.description = 'u can show modal when u need it'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_dependency 'rails', '~> 5.1.4'
  s.add_dependency 'jquery-rails', '4.3.1'
  s.add_dependency 'coffee-rails', '4.2.2'
  s.add_dependency 'bootstrap-sass', '3.3.7'
  s.add_dependency 'sass-rails', '~> 5.0'
end
