#= require jquery
#= require bootstrap/modal

window.LM ||= {}

LM.loadRemoteModalByID = (modalID, modalOptions) ->
  new LazyModalModal(modalID, modalOptions).show()

LM.triggerLoadRemoteModal = (event) ->
  event.preventDefault()
  $target = $(event.currentTarget)
  modalOptions = $target.data('modal-options')
  modalID = modalOptions.target || modalOptions.id || $target.attr('href')
  LM.loadRemoteModalByID modalID, modalOptions

class LazyModalModal
  constructor: (@originModalID, @modalOptions) ->
    @modalTitle = @modalOptions?.default_modal?.title || ''
    alert('u must set modal ID with data-target or href attrs') unless @originModalID
    if @originModalID.indexOf('#') !=-1
      @modalID = @originModalID?.split("#").pop()
    else
      @modalID = @originModalID
      @originModalID = "##{@originModalID}"

  show: ->
    return unless @modalID
    @_findOrCreateModal()
    @_showModal()

  _findOrCreateModal: ->
    if $(@originModalID).length
      @modal = $(@originModalID)
    else
      modalDom = @_createModalDom()
      @modal = $(modalDom)
      @_addModalEventListener()

  _showModal: ->
    @modal?.modal 'show' unless @modal?.isShown

  _createModalDom: ->
    """
      <div class="modal light dialog in" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog #{@modalOptions?.default_modal?.size || ''}">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal"><span>×</span></button>
	            <h4 class="modal-title">#{@modalTitle || ''}</h4>
	          </div>
	          <div class="modal-body clearfix">
	            <h5 class="text-center">loading...</h5>
	          </div>
	        </div>
        </div>
      </div>
    """

  _addModalEventListener: ->
    @modal.on 'shown.bs.modal', =>
      return if $("##{@modalID}").length
      data = {modal_dir: @modalOptions?.dir}
      console.log @modalID, @modalOptions
      $.get("/lazy_modals/#{@modalID}", data)
        .done (response) => @_loadRemoteModalSuccessed response
        .fail (response, xhr, status) =>
            @modal.find('.modal-body').html response.responseText

  _loadRemoteModalSuccessed: (response)->
    @modal?.modal 'hide'
    @modal = $(response)
    $('body').append @modal
    @modal.trigger 'LM.modal.content.loaded', @modalOptions
    @show()

$(document).on 'click', '[data-lazy-modal="true"]', LM.triggerLoadRemoteModal
