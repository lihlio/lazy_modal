module LazyModal
  class LazyModalsController < ActionController::Base
    protect_from_forgery with: :exception

    rescue_from ActionView::MissingTemplate do
      render plain: 'modal missing', status: :not_found
    end

    def show
      tmpl = if params[:modal_dir]
               "#{params[:modal_dir]}/#{params[:id]}"
             else
               "/lazy_modal/modals/#{params[:id]}"
             end
      render tmpl, layout: nil
    end
  end
end
